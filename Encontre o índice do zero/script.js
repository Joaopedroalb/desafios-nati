function indiceMaiorZero(){
    var str = document.getElementById("valorNum").value
    var arrayUns = str.split('0')
    var maiorSequencia = ""
    var indiceDoZero

    for(var i = 0; i<arrayUns.length-1;i++){
        if((arrayUns[i]+arrayUns[i+1]).length>maiorSequencia.length){
            indiceDoZero = i
            maiorSequencia = arrayUns[i]+arrayUns[i+1]
            
        }
    }
    var maiorIndice = 0
    var aux = 0;
    var indiceUndefined = true
    for(var j = 0; j<str.length; j++){
        if(str.charAt(j)==0&&indiceUndefined){
            if(aux == indiceDoZero){
                maiorIndice= j;
                indiceUndefined = false
            }else{
                aux++
            }
            
        }
    }
    document.getElementById("resposta").innerHTML = "O indice é "+(maiorIndice+1)
    document.getElementById("stringBefore").innerHTML= "String antes : "+str
    document.getElementById("stringAfter").innerHTML= "String depois : "+(str.substring(0,maiorIndice)+"1"+str.substring(maiorIndice+1,str.length))
}
