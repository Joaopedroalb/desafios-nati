var arr = [];
var indexAux = 0;

function adicionar(){
    let titulo = document.getElementById('titulo')
    if(titulo.value!=''){
        arr.push(titulo.value)
        gerar(arr)
    }   
    titulo.value = ""
}

function remover(id){
    arr.splice(id,1)
    gerar(arr)
}

function gerar(arr){
    let lista = document.getElementById('lista');
    lista.innerHTML = '';

    for(var i = 0; i < arr.length; i++){
        let item = document.createElement('li');
        let btnRemover = document.createElement("button");
        btnRemover.innerHTML="Remover";
        btnRemover.classList.add(indexAux)
        btnRemover.addEventListener ("click", function() {
            remover(btnRemover.className)
          })
        indexAux++
        item.appendChild(document.createTextNode(arr[i]));
        item.appendChild(btnRemover)
        lista.appendChild(item);
    }
    indexAux = 0
}
