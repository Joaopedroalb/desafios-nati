public class Cargo {
    private double salario;
    private String nomeCargo;

    public Cargo(double salario, String nomeCargo) {
        this.salario = salario;
        this.nomeCargo = nomeCargo;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getNomeCargo() {
        return nomeCargo;
    }

    public void setNomeCargo(String nomeCargo) {
        this.nomeCargo = nomeCargo;
    }
}
