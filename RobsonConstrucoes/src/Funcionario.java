public class Funcionario {
    private String nome;
    private int code;
    private int codeCargo;

    public Funcionario(String nome, int code, int codeCargo) {
        this.nome = nome;
        this.code = code;
        this.codeCargo = codeCargo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCodeCargo() {
        return codeCargo;
    }

    public void setCodeCargo(int codeCargo) {
        this.codeCargo = codeCargo;
    }
}
