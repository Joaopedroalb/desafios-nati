import java.util.*;


public class main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Funcionario> funcionarios = new ArrayList();
        ArrayList<Cargo> cargos = new ArrayList();
        boolean on = true;
        do{
            System.out.println("1 - Criar cargo");
            System.out.println("2 - Criar funcionario");
            System.out.println("3 - Listar funcionarios");
            System.out.println("4 - Valores totais de um cargo");
            System.out.println("5 - Sair do programa");
            int btn = sc.nextInt();
            switch (btn){
                case 1: criarCargos(cargos);
                break;

                case 2: criarFuncionario(funcionarios,cargos);
                break;

                case 3: mostrarFuncionarios(funcionarios,cargos);
                break;

                case 4: mostrarSalario(cargos,funcionarios);
                break;

                case 5:
                    System.out.println("Finalizando processo...");
                    on = false;
                    break;
            }
            System.out.println(" ");

        }while(on);

    }
    public static void criarFuncionario(ArrayList<Funcionario> funcionarios, ArrayList<Cargo> cargos){
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite o nome do Funcionario");
        String nome = sc.nextLine();
        int code;
        int cargo = -1;

        boolean codigoVerificado = false;
        do {
            System.out.println("Digite um codigo");
            code = sc.nextInt();
            codigoVerificado = true;
            for(int i = 0; i<funcionarios.size();i++){
                if(funcionarios.get(i).getCode()==code){
                    System.out.println("Esse codigo já existe");
                    i=funcionarios.size();
                    codigoVerificado =false;
                }
            }
        }while(!codigoVerificado);

        if(cargos.size()>0){
            boolean cargoVerificado = false;
            do{
                System.out.println("Digite o cargo de "+nome);
                for(int i = 0 ; i<cargos.size();i++){
                    System.out.println(i+" - "+cargos.get(i).getNomeCargo());
                }
                cargo = sc.nextInt();
                if(cargo>=0&&cargo<cargos.size()){
                    cargoVerificado = true;
                }else{
                    System.out.println("Digite um numero valido");
                }
            }while(!cargoVerificado);
        }else{
            System.out.println("Não existem cargos cadastrados");
        }
        Funcionario funcionario = new Funcionario(nome,code,cargo);
        funcionarios.add(funcionario);

    }

    public static void criarCargos(ArrayList<Cargo> cargos){
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite o nome do cargo que deseja criar: ");
        String nome = sc.nextLine();
        System.out.println("Digite o salario de "+nome);
        double salario = sc.nextDouble();
        Cargo cargo = new Cargo(salario,nome);
        System.out.println("O id do cargo "+nome+" é : "+cargos.size());
        cargos.add(cargo);
    }

    public static void mostrarFuncionarios(ArrayList<Funcionario> funcionarios, ArrayList<Cargo> cargos){
        if(funcionarios.size()>0){
            for(int i = 0; i<funcionarios.size();i++){
                Funcionario funcionario = funcionarios.get(i);
                System.out.println("Nome : "+funcionario.getNome()+" \n Codigo: "+funcionario.getCode()+
                        "\n Cargo: "+cargos.get(funcionario.getCodeCargo()).getNomeCargo()+
                        "\n Salario: "+cargos.get(funcionario.getCodeCargo()).getSalario());
                System.out.println("-------------------------------------------------------");
            }
        }else{
            System.out.println("Não existem funcionarios cadastrados");
        }
    }

    public static void mostrarSalario(ArrayList<Cargo> cargos, ArrayList<Funcionario> funcionarios){
        Scanner sc = new Scanner(System.in);
        if(cargos.size()>0){
            boolean on = true;
            do{
                double valorTotal = 0;
                System.out.println("Seleciona o cargo desejado");
                for(int i = 0; i<cargos.size();i++){
                    System.out.println(i+" - "+cargos.get(i).getNomeCargo());
                }
                System.out.println(cargos.size()+ " - sair ");
                int cargoSelecionado = sc.nextInt();
                if(cargoSelecionado>=0 && cargoSelecionado<cargos.size()){
                    for(int j = 0; j<funcionarios.size();j++){
                        if(funcionarios.get(j).getCodeCargo()==cargoSelecionado){
                            valorTotal += cargos.get(cargoSelecionado).getSalario();
                        }
                    }
                    System.out.println("Valor total do cargo de "+cargos.get(cargoSelecionado).getNomeCargo()+ " é de "+valorTotal);
                }else{
                    if(cargoSelecionado==cargos.size()){
                        System.out.println("Saindo...");
                        on = false;
                    }else{
                        System.out.println("Digite um cargo valido");
                    }
                }

            }while(on);
        }else{
            System.out.println("Nao existem cargos cadastrados");
        }
    }

}
